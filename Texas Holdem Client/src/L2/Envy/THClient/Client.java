package L2.Envy.THClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by berry on 11/29/2016.
 */
public class Client implements Runnable{
    private Socket socket;
    private Player player;
    private Game game;
    private PrintWriter out;
    private BufferedReader in;
    private ArrayList<String> announcements;
    private boolean connected;
    public Client(Socket socket,String name, int money){
        connected = true;
        this.socket = socket;
        this.player = new Player(this,name, money);
        this.game = new Game(this, player);
        announcements = new ArrayList<>();
        game.addPlayer(player);
        try{
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }catch (IOException e){
            System.out.println("Could not open stream.");
        }
    }

    @Override
    public void run() {
        try{
            sendCommand("{new:"+ player.getName() + ":" + player.getMoney() + "}");
            String input;
            while(socket.isConnected()){
                if((input = in.readLine()) != null){
                    handleInput(input);
                }
            }
        }catch (IOException e){
            connected = false;
            System.out.println("Connection has been lost.");
        }
    }
    public boolean isConnected(){
        return connected;
    }
    public void handleInput(String input){
        String recieve = input;
        recieve = recieve.substring(1, recieve.length()-1);
        String[] subs = recieve.split(":");
        switch(subs[0]){
            //{update:pot:currentbet:field:[name;money;currentlybet;amntneeded;hand;ingame;turn;allin],[name;money;handingame;turn],[name;money;hand;ingame;turn]:(remove)[name],[name]}
            case "update":
                game.setPot(Integer.parseInt(subs[1]));
                game.setCurrentbet(Integer.parseInt(subs[2]));
                game.setField(subs[3]);
                //player stuff
                String[] playerdata = subs[4].split(",");
                for(String str : playerdata){
                    str = str.substring(1,str.length()-1);
                    String[] metadata = str.split(";");
                    Player player = game.getPlayer(metadata[0]);
                    if(player == null){
                        player = new Player(this, metadata[0], Integer.parseInt(metadata[1]));
                        game.addPlayer(player);
                    }
                    player.setMoney(Integer.parseInt(metadata[1]));
                    player.setCurrentlybet(Integer.parseInt(metadata[2]));
                    player.setMustadd(Integer.parseInt(metadata[3]));
                    player.setHand(metadata[4]);
                    player.setIngame(Boolean.parseBoolean(metadata[5]));
                    player.setTurn(Boolean.parseBoolean(metadata[6]));
                    player.setAllin(Boolean.parseBoolean(metadata[7]));
                }
                game.setUpdateScreen(true);
                break;
            case "msg":
                if(announcements.size() == 5){

                    announcements.add(subs[1]);
                }else {
                    announcements.add(subs[1]);
                }
                System.out.println(subs[1]);
                game.setUpdateScreen(true);
                break;
        }
    }
    public void sendCommand(String command){
        out.println(command);
    }
    public Game getGame(){
        return  game;
    }
    public Player getPlayer(){
        return player;
    }
    public List<String> getAnnouncements(){
        return announcements;
    }
}
