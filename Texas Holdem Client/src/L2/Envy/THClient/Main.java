package L2.Envy.THClient;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException{
        Socket socket;
        Client client;
        Scanner scanner = new Scanner(System.in);
        String name;
        int money;
        boolean quit = false;
        //aquire name and money.
        System.out.println("Welcome to Texas Holdem Online!");
        System.out.println("Please enter your name.");
        name = scanner.nextLine();
        System.out.println("Please enter the amount of money you are bringing with you.");
        money = scanner.nextInt();
        System.out.println("Please wait while we attempt to connect you to the server...");
        while(!quit){
            try{
                socket = new Socket("127.0.0.1",4444);
                client = new Client(socket, name, money);
                new Thread(client).start();
                while(client.isConnected()){
                    Thread.sleep(1000);
                    if(client.getGame().shouldUpdateScreen()){
                        clrscr();
                        System.out.println(client.getGame().getScreen());
                        client.getGame().setUpdateScreen(false);
                        if(client.getPlayer().isTurn()){
                            boolean complete = false;
                            while(!complete) {
                            System.out.println("You have bet " + client.getPlayer().getCurrentlybet() + " so far.");
                            if(client.getPlayer().getMustadd() > 0){
                                System.out.println("You must bet: " + client.getPlayer().getMustadd());
                            }
                            System.out.println("Choose an option:");
                            System.out.println("1) Fold");
                            if (client.getPlayer().getMustadd() == 0) {
                                System.out.println("2) Check");
                                System.out.println("3) Raise");
                            } else {
                                System.out.println("2) Call");
                                System.out.println("3) Raise");
                            }
                                int option = scanner.nextInt();
                                switch (option) {
                                    case 1:
                                        client.sendCommand("{fold}");
                                        complete = true;
                                        break;
                                    case 2:
                                        if(client.getPlayer().getMustadd() == 0){
                                            client.sendCommand("{check}");
                                            complete = true;
                                        }else{
                                            int amount = client.getPlayer().getMustadd();
                                            if(amount >= client.getPlayer().getMoney()){
                                                System.out.println("You will be going all in! Are you sure you would like to do this?");
                                                complete = scanner.nextBoolean();
                                                if(complete){
                                                    client.sendCommand("{allin}");
                                                }
                                            }else{
                                                client.sendCommand("{call}");
                                                complete = true;
                                            }
                                        }
                                        break;
                                    case 3:
                                        System.out.println("How much would you like to raise?");
                                        int amount = scanner.nextInt();
                                        amount += client.getPlayer().getMustadd();
                                        if(amount >= client.getPlayer().getMoney()){
                                            System.out.println("You will be going all in! Are you sure you would like to do this?");
                                            complete = scanner.nextBoolean();
                                            if(complete){
                                                client.sendCommand("{allin}");
                                            }
                                        }else{
                                            client.sendCommand("{raise:" + amount + "}");
                                            complete = true;
                                        }
                                        break;
                                    default:
                                        System.out.println("that was not an option");
                                        break;

                                }
                            }
                        }
                    }
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
    public static void clrscr(){
        //Clears Screen in java
        try {
            if (System.getProperty("os.name").contains("Windows"))
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            else
                Runtime.getRuntime().exec("clear");
        } catch (IOException | InterruptedException ex) {}
    }
}
