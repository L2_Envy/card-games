package L2.Envy.THClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by berry on 11/29/2016.
 */
public class Game {
    private int pot;
    private int currentbet;
    private String field;
    private Client client;
    private ArrayList<Player> playerArrayList = new ArrayList<>();
    private Player user;
    private boolean updateScreen;
    public Game(Client client, Player player){
        this.client = client;
        pot = 0;
        currentbet = 0;
        field = "[][][][][]";
        updateScreen = false;
        this.user = player;
    }
    public void setUpdateScreen(boolean updateScreen){
        this.updateScreen = updateScreen;
    }
    public boolean shouldUpdateScreen(){
        return updateScreen;
    }
    public String getScreen(){
        String screen = "";
        for(String str : client.getAnnouncements()){
            screen = screen + str + "\n";
        }
        screen = screen + "Pot: " + pot + "\n";
        screen = screen + "Current bets: " + currentbet + "\n";
        screen = screen + field + "\n\n";
        for(Player player : playerArrayList){
            if(player.isIngame()){
                if(player.isTurn()){
                    screen = screen + "*";
                }
                if(player.isAllin()){
                    screen = screen + "ALL IN ";
                }
                screen = screen + player.getName() + ": " + player.getMoney() + "\n";
                if(player.getName().equalsIgnoreCase(user.getName())) {
                    screen = screen + player.getHand() + "\n";
                }else{
                    screen = screen + "[][]" + "\n";
                }
            }
        }
        return screen;
    }
    public int getCurrentbet(){
        return currentbet;
    }
    protected void setPot(int pot){
        this.pot = pot;
    }
    protected void setCurrentbet(int currentbet){
        this.currentbet = currentbet;
    }
    protected void setField(String field){
        this.field = field;
    }
    protected Player getPlayer(String name){
        for(Player player : playerArrayList){
            if(player.getName().equalsIgnoreCase(name)){
                return player;
            }
        }
        return null;
    }
    protected void addPlayer(Player player){
        playerArrayList.add(player);
    }
}
