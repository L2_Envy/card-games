package L2.Envy.THClient;

import java.util.HashMap;

/**
 * Created by berry on 11/29/2016.
 */
public class Player {
    private String name;
    private int money;
    private int mustadd;
    private int currentlybet;
    private Client client;
    private String hand;
    private boolean turn;
    private boolean ingame;
    private boolean allin;
    public Player(Client client, String name, int money){
        this.name = name;
        this.money = money;
        this.client = client;
        mustadd = 0;
        currentlybet = 0;
        hand = "[][]";
        turn = false;
        ingame = false;
        allin = false;
    }
    public String getName(){
        return name;
    }
    public int getMoney(){
        return money;
    }
    public int getMustadd(){
        return mustadd;
    }
    public int getCurrentlybet(){
        return currentlybet;
    }
    public String getHand(){
        return hand;
    }
    public boolean isTurn(){
        return turn;
    }
    public boolean isIngame(){
        return ingame;
    }
    public boolean isAllin(){
        return allin;
    }
    public void setHand(String hand){
        this.hand = hand;
    }
    public void setTurn(boolean turn){
        this.turn = turn;
    }
    public void setAllin(boolean allin){
        this.allin = allin;
    }

    public void setIngame(boolean ingame) {
        this.ingame = ingame;
    }

    public void setMoney(int money){
        this.money = money;
    }
    public void setMustadd(int mustadd){
        this.mustadd = mustadd;
    }
    public void setCurrentlybet(int currentlybet){
        this.currentlybet = currentlybet;
    }
}
