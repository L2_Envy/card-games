package L2.Envy.THServer;

import L2.Envy.THServer.GameManager.Game;
import L2.Envy.THServer.GameManager.Player;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by berry on 11/29/2016.
 */
public class Server implements Runnable{
    private ServerSocket serverSocket;
    private ArrayList<Client> clients;
    private Game game;
    private boolean online;
    public Server(){
        clients = new ArrayList<>();
        game = new Game(this);
    }
    public void startServer(){
        try{
            serverSocket = new ServerSocket(4444);
            System.out.println("Server started on port 4444.");
            online = true;
        }catch (IOException e){
            e.printStackTrace();
            System.out.println("Error starting server.");
        }
    }

    @Override
    public void run() {
        while(acceptClient()){
            System.out.println("New Client connected.");
        }
    }
    public boolean acceptClient(){
        try{
            Socket socket = serverSocket.accept();
            if(socket != null){
                Client client = new Client(this, socket);
                clients.add(client);
                new Thread(client).start();
                return true;
            }
        }catch (IOException e){
            System.out.println("Client was unable to connect to server.");
        }
        return false;
    }
    public void removeClient(Client client){
        if(clients.contains(client)){
            clients.remove(client);
        }
    }
    public void createGameThread(){
        new Thread(game).start();
    }
    protected Game getGame(){
        return game;
    }
    public boolean isOnline() {
        return online;
    }
    public void announce(String message){
        for(Client client : clients){
            client.sendCommand("{msg:" + message + "}");
        }
    }
    public void sendUpdate(){
        String str = "{update:" + game.getPot() + ":" + game.getCurrentbet() + ":" + game.getField() + ":";
        for(Client client : clients){
            Player player = client.getPlayer();
            str = str + "[" + player.getName() + ";" + player.getMoney() + ";" + player.getCurrentlybet() + ";" + player.getMustbet() + ";" + player.getHandStr() +";"+player.isIngame()+";"+player.isTurn()+";"+ player.isAllin() +"],";
        }
        str = str.substring(0, str.length()-1);
        str = str + "}";
        System.out.println(str);
        for(Client client : clients){
            client.sendCommand(str);
        }
    }
}
