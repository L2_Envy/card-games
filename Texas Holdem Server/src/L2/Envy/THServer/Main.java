package L2.Envy.THServer;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Initializing Texas Holdem Online Server...");
        Server server = new Server();
        server.startServer();
        new Thread(server).start();
        while(server.isOnline()){
            Thread.sleep(1000);
        }
    }
}
