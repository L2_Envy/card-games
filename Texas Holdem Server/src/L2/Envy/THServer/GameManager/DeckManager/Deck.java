package L2.Envy.THServer.GameManager.DeckManager;

import java.util.Collections;
import java.util.Stack;

/**
 * Created by berry on 11/29/2016.
 */
public class Deck {
    private Stack<Card> deck;
    private Stack<Card> discarddeck;
    public Deck(){
        createDeck();
        shuffle();
    }
    public void createDeck(){
        deck = new Stack<>();
        discarddeck = new Stack<>();
        for(int i = 0; i < 4; i++){
            switch(i){
                case 0:
                    for(int j = 1; j < 14; j++){
                        deck.push(new Card(Suit.HEART, j));
                    }
                    break;
                case 1:
                    for(int j = 1; j < 14; j++){
                        deck.push(new Card(Suit.SPADE, j));
                    }
                    break;
                case 2:
                    for(int j = 1; j < 14; j++){
                        deck.push(new Card(Suit.DIAMOND, j));
                    }
                    break;
                case 3:
                    for(int j = 1; j < 14; j++){
                        deck.push(new Card(Suit.CLUB, j));
                    }
                    break;
                default:
                    break;
            }
        }
    }
    public void resetDeck(){
        createDeck();
        shuffle();
    }
    public void shuffle(){
        Collections.shuffle(deck);
    }
    public Card pullCard(){
        return deck.pop();
    }
    public void discard(){
        deck.pop();
    }
    public String toString(){
        String str = "";
        int count = 1;
        while(!deck.isEmpty()){
            str = str + count + ") " +deck.pop().getCard();
            str = str + "\n";
            count ++;
        }
        return str;
    }
}
