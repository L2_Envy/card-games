package L2.Envy.THServer.GameManager;

import L2.Envy.THServer.GameManager.DeckManager.Card;
import L2.Envy.THServer.GameManager.DeckManager.Deck;
import L2.Envy.THServer.GameManager.DeckManager.Suit;
import L2.Envy.THServer.Server;

import java.io.IOException;
import java.util.*;
import java.util.ArrayList;

/**
 * Created by berry on 11/29/2016.
 */
public class Game implements Runnable {
    private Server server;
    private Deck deck;
    private ArrayList<Card> field;
    private int pot;
    private int currentbet;
    private ArrayList<Player> players;
    //[A][10][Q][K][]
    //[J][]
    public Game(Server server) {
        this.server = server;
        pot = 0;
        currentbet = 0;
        deck = new Deck();
        players = new ArrayList<>();
        field = new ArrayList<>();
    }

    public void addPlayer(Player player) {
        players.add(player);
        if (players.size() == 1) {
            server.createGameThread();
        }
        server.sendUpdate();
        server.announce("Player " + player.getName() + " has joined!");
    }

    public void removePlayer(Player player) {
        if (players.contains(player)) {
            players.remove(player);
        }
        server.sendUpdate();
    }

    ///game thread
    @Override
    public void run() {
        //Game loop
        while (players.size() > 0) {
            //start game
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {

            }
            if (players.size() >= 2) {
                for (Player player : players) {
                    if (player.getMoney() > 0) {
                        player.setTurn(false);
                        player.setAllin(false);
                        player.setIngame(true);
                        player.addCard(deck.pullCard());
                    } else {
                        player.setIngame(false);
                        player.setTurn(false);
                    }
                }
                for (Player player : players) {
                    if (player.isIngame()) {
                        player.addCard(deck.pullCard());
                    }
                }
                server.announce("The game is starting!");
                for (int i = 0; i < 4; i++) {
                    currentbet = 0;
                    handleBetting();
                    server.sendUpdate();
                    switch (i) {
                        case 0:
                            deck.discard();
                            field.add(deck.pullCard());
                            field.add(deck.pullCard());
                            field.add(deck.pullCard());
                            break;
                        case 1:
                            deck.discard();
                            field.add(deck.pullCard());
                            break;
                        case 2:
                            deck.discard();
                            field.add(deck.pullCard());
                            break;
                        case 3:
                            //end game
                            determinePot();
                            deck.resetDeck();
                            field = new ArrayList<>();
                            for(Player player : players){
                                player.clearHand();
                            }
                            try {
                                Thread.sleep(10000);
                            }catch (InterruptedException e){

                            }
                            break;
                    }
                }
                //Update all players
            }
        }
    }

    public int getPot() {
        return pot;
    }

    public int getCurrentbet() {
        return currentbet;
    }

    public String getField() {
        String str = "";
        if (!field.isEmpty()) {
            for (Card card : field) {
                str = str + "[" + card.getCard() + "]";
            }
        } else {
            return "[][][][][]";
        }
        return str;
    }

    private boolean allowedToBet(Player player) {
        if (player.isIngame()) {
            if (!player.isAllin()) {
                //add more check if folded, or opponents are all in
                for (Player player1 : players) {
                    if (!player.getName().equalsIgnoreCase(player1.getName())) {
                        if (!player1.isAllin() && player1.isIngame()) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private void handleBetting() {
        //initial bets
        for (Player player : players) {
            if (player.isIngame()) {
                if (!player.isAllin()) {
                    if (allowedToBet(player)) {
                        player.setTurn(true);
                        server.sendUpdate();
                        while (player.isTurn()) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {

                            }
                        }
                        server.sendUpdate();
                    }
                }
            }
        }
        //sesquential bets
        while (!allBetsDone()) {
            for (Player player : players) {
                if (player.isIngame()) {
                    if (!player.isAllin()) {
                        //CHECK HERE IF SHOULD BE ALLOWED TO BET, OR SKIP
                        if (allowedToBet(player)) {
                            if (player.getMustbet() > 0) {
                                player.setTurn(true);
                                server.sendUpdate();
                                while (player.isTurn()) {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {

                                    }
                                }
                                server.sendUpdate();
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean allBetsDone() {
        for (Player player : players) {
            if (player.isIngame()) {
                if (!player.isAllin()) {
                    if (player.getMustbet() > 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public void bid(Player player, int amount) {
        //Add to pot
        pot = pot + amount;
        //increase currently bet
        player.setCurrentlybet(player.getCurrentlybet() + amount);
        //if all in, set money and owed to 0;
        if (player.isAllin()) {
            player.setMoney(0);
            player.setMustbet(0);
        } else {
            //if not all in, go ahead an subtract amount from money.
            player.setMoney(player.getMoney() - amount);
            //check if what they must bet, is eqal to the amount they are betting
            if (player.getMustbet() == amount) {
                //if equal, set must bet to 0
                player.setMustbet(0);
                //if they are betting than what they must, then they are raising.
            } else if (player.getMustbet() < amount) {
                //get the amount they are raising by
                int leftover = amount - player.getMustbet();
                //for each player, add in what they must bet.
                for (Player player1 : players) {
                    //as long as the player is not the one that bet.
                    if (!player1.getName().equalsIgnoreCase(player.getName())) {
                        player1.setMustbet(player1.getMustbet() + leftover);
                    }
                }
                //Update what the current bet to reach is.
                currentbet = currentbet + leftover;
                //if the player bets less than what they must bet,
                //subtract it from what they must bet to get a leftover.
            } else if (player.getMustbet() > amount) {
                player.setMustbet(player.getMustbet() - amount);
            }
        }
    }

    public void determinePot() {
        boolean tie = false;
        int rank = 0;
        ArrayList<Player> tempwinner = new ArrayList<>();
        Player winner = null;

        for(Player player : players){
            ArrayList<Card> temphand = new ArrayList<>();
            temphand.addAll(field);
            temphand.addAll(player.getHand());
            Collections.sort(temphand, new Card.CompId());
            int temp = getRank(temphand);
            if(temp > rank){
                rank = temp;
                tempwinner.clear();
                tempwinner.add(player);
            }else if(temp == rank){
                tempwinner.add(player);
            }
            if(tempwinner.size() == 1){
                winner = tempwinner.get(0);
            }else{
                winner = getTieBreaker(tempwinner);
            }
        }
        winner.setMoney(winner.getMoney() + pot);
        server.announce(winner.getName().toUpperCase() + " IS THE WINNER! HE WON $" + pot);
        switch (rank){
            case 10:
                server.announce(winner.getName() + " won with a Royal Flush!");
                break;
            case 9:
                server.announce(winner.getName() + " won with a Straight Flush!");
                break;
            case 8:
                server.announce(winner.getName() + " won with a Four of a Kind!");
                break;
            case 7:
                server.announce(winner.getName() + " won with a Full House!");
                break;
            case 6:
                server.announce(winner.getName() + " won with a Flush!");
                break;
            case 5:
                server.announce(winner.getName() + " won with a Straight!");
                break;
            case 4:
                server.announce(winner.getName() + " won with a Three of a Kind!");
                break;
            case 3:
                server.announce(winner.getName() + " won with Two Pair!");
                break;
            case 2:
                server.announce(winner.getName() + " won with a One Pair!");
                break;
            case 1:
                server.announce(winner.getName() + " won with a High Card!");
                break;
            case 0:
                server.announce(winner.getName() + " won with nothing!");
                break;
        }
        pot = 0;
        currentbet = 0;
    }

    private int getRank(ArrayList<Card> hand) {
        int highestrank = 0;
        if (hand.size() == 7) {
            if (isRoyalFlush(hand)) {
                highestrank = 10;
            } else if (isStraightFlush(hand)) {
                highestrank = 9;
            } else if (isFourOfAKind(hand)) {
                highestrank = 8;
            } else if (isFullHouse(hand)) {
                highestrank = 7;
            } else if (isFlush(hand)) {
                highestrank = 6;
            } else if (isStraight(hand)) {
                highestrank = 5;
            } else if (isThreeOfAKind(hand)) {
                highestrank = 4;
            } else if (isTwoPair(hand)) {
                highestrank = 3;
            } else if (isPair(hand)) {
                highestrank = 2;
            } else if (isHighCard(hand)) {
                highestrank = 1;
            }
        }
        return highestrank;
    }

    private boolean isRoyalFlush(ArrayList<Card> hand) {
        if (isFlush(hand)) {
            ArrayList<Card> flush = getFlush(hand);
            boolean hasA= false;
            boolean hasK= false;
            boolean hasQ= false;
            boolean hasJ= false;
            boolean has10= false;
            for(int i = 0; i < flush.size(); i++){
                switch (flush.get(i).getValue()){
                    case 1:
                        hasA = true;
                        break;
                    case 13:
                        hasK = true;
                        break;
                    case 12:
                        hasQ = true;
                        break;
                    case 11:
                        hasJ = true;
                        break;
                    case 10:
                        has10 = true;
                        break;
                }
            }
            return (has10 && hasA && hasJ && hasK && hasQ);
        }
        return false;
    }

    private boolean isStraightFlush(ArrayList<Card> hand) {
        if (isFlush(hand)) {
            ArrayList<Card> flush = getFlush(hand);
            return isStraight(flush);
        }
        return false;
    }

    private boolean isFourOfAKind(ArrayList<Card> hand) {
       for(int i = 0; i < hand.size(); i++){
           int temp = hand.get(i).getValue();
           int count = 1;
           for(int j = 0; j < hand.size(); j++){
               if(i != j){
                   if(temp == hand.get(j).getValue()){
                       count++;
                       if(count == 4){
                           return true;
                       }
                   }
               }
           }
       }
       return false;
    }

    private boolean isFullHouse(ArrayList<Card> hand) {
        for (Card card : hand) {
            for (Card card1 : hand) {
                if (!card1.equals(card)) {
                    for (Card card2 : hand) {
                        if (!card2.equals(card) && !card2.equals(card1)) {
                            if (card.getValue() == card1.getValue() && card.getValue() == card2.getValue()) {
                                for (Card card3 : hand) {
                                    if (!card3.equals(card) && !card3.equals(card1) && !card3.equals(card2)) {
                                        for (Card card4 : hand) {
                                            if (!card4.equals(card) && !card4.equals(card1) && !card4.equals(card2) && !card4.equals(card3) && card3.getValue() == card4.getValue()) {
                                                return true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean isFlush(ArrayList<Card> hand) {
        for(int i = 0; i < hand.size(); i++){
            Suit temp = hand.get(i).getSuit();
            int count = 1;
            for(int j = 0; j < hand.size(); j++){
                if(i != j){
                    if(temp == hand.get(j).getSuit()){
                        count++;
                        if(count == 5){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean isStraight(ArrayList<Card> hand) {
        //when the hand is passed into the method, it is organized by numerical value. Biggest - Smallest
        int count,last = 0;
        //iterate through whole hand (7 Cards total)
        for(int j = 0; j < hand.size(); j++){
            //count will keep track of how many we have in a row.
            count = 0;
            //last will keep track of the previous value card.
            last = hand.get(j).getValue();
            //index is what the card index is.
            int index = j;
            //The first iteration is the "starting" card.
            // The second iteration will be how many cards are in a straight after the starting card
            for(int i = 0; i < hand.size(); i++){
                //Card is the card to check against the previous value.
                Card card = null;
                //If card index + what iteration we are on is greater than 7, then we must loop back.
                if(index + i > hand.size()-1){
                    //grab the modulus of the index + i
                    int leftover = (index +i) % hand.size();
                    //set the card to be this.
                    card = hand.get(leftover);
                }else{
                    //if we are not over, go ahead and grab the card.
                    card = hand.get(index + i);
                }
                //Using an if-then, if the current card, is back at the beginning then..
                if(card.getValue() ==  hand.get(0).getValue()){
                    //the current card must be 13., and the last card must be a 1.
                    if(card.getValue() == 13 && last == 1){
                        //if it is, then it is part of a straight, increment
                        count++;
                        //if we hit 5, then the player must have a straight
                        if(count == 5){
                            return true;
                        }else {
                            //if not, then set the last to the card value current.
                            last = card.getValue();
                        }
                        //if card value == last, then we skip it.
                    }if(card.getValue() == last) {
                        //do nothing
                    }else{
                        //if it is neither above, the straight is broken.
                        count = 0;
                    }
                }else{
                    //if card is equal to the last-1 then, it is part of the straight.
                    if(card.getValue() == last-1){
                        //if it is, then we increment
                        count++;
                        //if count = 5 then we know the straight is finished.
                        if(count == 5){
                            return true;
                        }else{
                            //if not, we set the last card to the current card.
                            last = card.getValue();
                        }

                    }else if(card.getValue() == last) {
                        //do nothing
                    }else{
                        //if it is neither, then we reset becasue the count is broken.
                        count = 0;
                        break;
                    }
                }
            }
        }
        return false;
    }

    private boolean isThreeOfAKind(ArrayList<Card> hand) {
        for(int i = 0; i < hand.size(); i++){
            int temp = hand.get(i).getValue();
            int count = 1;
            for(int j = 0; j < hand.size(); j++){
                if(i != j){
                    if(temp == hand.get(j).getValue()){
                        count++;
                        if(count == 3){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean isTwoPair(ArrayList<Card> hand) {
        for (Card card : hand) {
            for (Card card1 : hand) {
                if (card.getValue() == card1.getValue()) {
                    for (Card card2 : hand) {
                        if (!card2.equals(card) && !card2.equals(card1)) {
                            for (Card card3 : hand) {
                                if (!card3.equals(card) && !card2.equals(card1) && !card3.equals(card2) && card3.getValue() == card2.getValue()) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean isPair(ArrayList<Card> hand) {
        for(int i = 0; i < hand.size(); i++){
            int temp = hand.get(i).getValue();
            int count = 1;
            for(int j = 0; j < hand.size(); j++){
                if(i != j){
                    if(temp == hand.get(j).getValue()){
                        count++;
                        if(count == 2){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean isHighCard(ArrayList<Card> hand) {
        int highest;
        return true;
    }

    private Player getTieBreaker(ArrayList<Player> players) {
        Player winner = null;
        int heighest = 0;
        for(Player player : players){
            ArrayList<Card> temphand = new ArrayList<>();
            temphand.addAll(field);
            temphand.addAll(player.getHand());
            Collections.sort(temphand, new Card.CompId());
            int temp = 0;
            for(int i = 0; i < 5; i++){
                temp += temphand.get(i).getValue();
            }
            if(temp > heighest){
                winner = player;
                heighest = temp;
            }

        }
        return winner;
    }
    private ArrayList<Card> getFlush(ArrayList<Card> hand){
        ArrayList<Card> flush =  new ArrayList<>();
        for(int i = 0; i < hand.size(); i++){
            flush.clear();
            Suit temp = hand.get(i).getSuit();
            flush.add(hand.get(i));
            for(int j = 0; j < hand.size(); j++){
                if(i != j){
                    if(temp == hand.get(j).getSuit()){
                        flush.add(hand.get(j));
                    }
                }
            }
            if(flush.size() >= 5){
                return flush;
            }
        }
        return null;
    }

}
