package L2.Envy.THServer.GameManager;

import L2.Envy.THServer.Client;
import L2.Envy.THServer.GameManager.DeckManager.Card;
import L2.Envy.THServer.Server;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by berry on 11/29/2016.
 */
public class Player {
    private Client client;
    private Server server;
    private Game game;
    private String name;
    private int money;
    private int currentlybet;
    private int mustbet;
    private Set<Card> hand;
    private boolean turn;
    private boolean ingame;
    private boolean allin;
    public Player(Server server, Client client,Game game, String name, int money){
        this.server = server;
        this.client = client;
        this.game = game;
        this.name = name;
        this.money = money;
        hand = new HashSet<>();
        ingame = false;
        turn = false;
        allin = false;
    }
    public void addCard(Card card){
        hand.add(card);
    }

    public String getHandStr(){
        String str = "";
        if(!hand.isEmpty()) {
            for (Card card : hand) {
                str = str + "["+card.getCard()+"]";
            }
        }else{
            return "[][]";
        }
        return str;
    }

    public boolean isAllin() {
        return allin;
    }

    public boolean isIngame() {
        return ingame;
    }

    public boolean isTurn() {
        return turn;
    }
    public String getName(){
        return name;
    }
    public int getMoney(){
        return money;
    }

    public int getCurrentlybet() {
        return currentlybet;
    }

    public int getMustbet() {
        return mustbet;
    }

    public void setTurn(boolean turn){
        this.turn = turn;
    }
    public void setAllin(boolean allin){
        this.allin = allin;
    }
    public void setIngame(boolean ingame){
        this.ingame = ingame;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void setCurrentlybet(int currentlybet) {
        this.currentlybet = currentlybet;
    }

    public void setMustbet(int mustbet) {
        this.mustbet = mustbet;
    }
    public void clearHand(){
        hand = new HashSet<>();
    }
    public Set<Card> getHand(){
        return hand;
    }
}
