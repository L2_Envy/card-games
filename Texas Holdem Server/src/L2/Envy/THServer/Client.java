package L2.Envy.THServer;

import L2.Envy.THServer.GameManager.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.Buffer;

/**
 * Created by berry on 11/29/2016.
 */
public class Client implements Runnable {
    private Server server;
    private Socket socket;
    private Player player;
    private BufferedReader in;
    private PrintWriter out;
    public Client(Server server, Socket socket){
        this.server = server;
        this.socket = socket;
        try{
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
        }catch (IOException e){
            System.out.println("Could not setup streams.");
        }
    }

    @Override
    public void run() {
        try{
            String input;
            while(socket.isConnected()){
                if((input = in.readLine()) != null){
                    handleInput(input);
                }
            }
            in.close();
            out.close();
            server.removeClient(this);
        }catch (IOException e){
            System.out.println("Client disconnected.");
            server.getGame().removePlayer(player);
            server.removeClient(this);
        }
    }
    public void handleInput(String input){
        String recieve = input;
        recieve = recieve.substring(1, recieve.length()-1);
        String[] subs = recieve.split(":");
        switch(subs[0]){
            case "new":
                player = new Player(server,this,server.getGame(),subs[1], Integer.parseInt(subs[2]));
                server.getGame().addPlayer(player);
                break;
            case "fold":
                player.setIngame(false);
                player.clearHand();
                player.setTurn(false);
                break;
            case "check":
                server.getGame().bid(player,0);
                player.setTurn(false);
                server.announce(player.getName() + " has checked!");
                break;
            case "call":
                server.getGame().bid(player, player.getMustbet());
                player.setTurn(false);
                server.announce(player.getName() + " has called!");
                break;
            case "raise":
                int amount = Integer.parseInt(subs[1]);
                server.getGame().bid(player, amount);
                player.setTurn(false);
                server.announce(player.getName() + " has raised the current bet by " + amount +"!");
                break;
            case "allin":
                player.setAllin(true);
                server.getGame().bid(player,player.getMoney());
                player.setTurn(false);
                server.announce(player.getName() + " is now all in!");
                break;
            case "msg":
                break;
        }
    }
    public void sendCommand(String cmd){
        out.println(cmd);
    }
    public Player getPlayer(){
        return  player;
    }
}
