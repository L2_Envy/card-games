package L2.Envy.Sol;

import L2.Envy.Sol.DeckManager.Deck;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Field field = new Field(new Deck());
        System.out.println("Welcome to solitare");
        System.out.println("How to play:");
        System.out.println("#,#,#");
        System.out.println("First number is the index of the stack you are taking from");
        System.out.println("Second number is how many cards in.");
        System.out.println("Third number is the stack you wish to move the cards to");
        boolean quit = false;
        while(!quit){
            System.out.println(field.displayField());
            String[] moves = scanner.nextLine().split(",");
            if(field.movePlayable(Integer.parseInt(moves[2]), Integer.parseInt(moves[0]), Integer.parseInt(moves[1]))){
                System.out.println("You cannot do that!");
            }
        }
    }
}
