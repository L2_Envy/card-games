package L2.Envy.Sol;

import L2.Envy.Sol.DeckManager.Card;
import L2.Envy.Sol.DeckManager.Deck;
import L2.Envy.Sol.DeckManager.Suit;

/**
 * Created by berry on 12/29/2016.
 */
public class Field {
    private Card[][] facedown;
    private Card[][] faceup;
    private Card[][] aces;
    private Deck deck;
    private Card[] playable;
    public Field(Deck deck){
        this.deck = deck;
        setupField();
        pullThree();
    }
    private void setupField(){
        facedown = new Card[7][7];
        faceup = new Card[7][13];
        playable = new Card[3];
        aces = new Card[4][13];
        for(int j = 1; j < 7; j++) {
            for (int i = 0; i < j; i++) {
                facedown[j][i]= deck.pullCard();
            }
        }
        for(int i = 0; i < 7; i++){
            faceup[i][0] = deck.pullCard();
        }
    }
    //Pulls three new cards and puts the old ones back in the deck
    public void pullThree(){
        for(int i = 2; i > -1; i--){
            if(playable[i] != null) {
                deck.discardCard(playable[i]);
            }
        }
        if(deck.deckEmpty()){
            deck.reAddDiscard();
        }
        for(int i = 0; i < 3; i++){
            playable[i] = deck.pullCard();
        }
    }
    //handle moving card and cleaning up. As well as flipping cards... Split this up
    public boolean movePlayable(int tostack, int fromstack, int starting){
        Card[] cards = new Card[13];//WIll be all the cards to be moved
        //Figure out what stack.. if main field
        if(fromstack>= 5 && fromstack <=11){
            Card[] stack = faceup[fromstack-5];//Subtract 5
            System.out.println("Grabbed stack");
            printStack(stack);
            //get starting card index;
            int index = getLastCardIndex(stack) -starting;
            System.out.println("Got index: " + index);
            //if starting card is allowed to go to next pile, the rest are allowed aswell
            if(tryCard(tostack, stack[index])){
                System.out.println("Card allowed");
                //grab all cards to be moved, remove from field.
                for(int i = index; i < index+starting + 1; i++) {
                    for (int j = 0; j < 13; j++) {
                        if (cards[j] == null) {
                            cards[j] = stack[i];
                            faceup[fromstack-5][i] = null;
                            j = 13;
                        }
                    }
                }
                System.out.println("Old stack:");
                printStack(faceup[fromstack-5]);
                System.out.println("Grabbed cards:");
                printStack(cards);
                //now that cards are removed off field, see if a card needs to be flipped
                if(index == 0){
                    int lastcardindex = getLastCardIndex(facedown[fromstack-5]);
                    if(lastcardindex >-1){
                        faceup[fromstack-5][0] = facedown[fromstack-5][lastcardindex];
                        facedown[fromstack-5][lastcardindex] = null;
                    }
                }
                System.out.println("Old Stack after flip");
                printStack(faceup[fromstack-5]);
                //time to move cards to new deck
                System.out.println();
                if(tostack >= 1 && tostack <= 4){
                    //aces
                    index = getLastCardIndex(aces[tostack-1]) + 1;
                    for (int i = 0; i < 13; i++) {
                        if (cards[i] != null) {
                            aces[tostack - 1][i + index] = cards[i];
                        }
                    }
                    printStack(aces[tostack-1]);
                }else if(tostack >= 5 && tostack <= 11) {
                    //other
                    index = getLastCardIndex(faceup[tostack - 5]) + 1;
                    for (int i = 0; i < 13; i++) {
                        if (cards[i] != null) {
                            faceup[tostack - 5][i + index] = cards[i];
                        }
                    }
                    printStack(faceup[tostack-5]);
                }
                //Done moving
            }
        }else if(fromstack == 12){//Handle playable hand

        }
        return false;
    }
    //Prints out an array
    private void printStack(Card[] card){
        String str = "";
        for(int i =0; i < card.length; i++){
            if(card[i] != null){
                str = str + "[" + card[i].getCard() + "]";
            }
        }
        System.out.println(str);
    }
    //aces 1-4,hand 5-11, Check if card will go in stack
    private boolean tryCard(int stack, Card card){
        //aces
        if(stack >= 1 && stack <= 4){
            Card card1 = getLastCard(aces[stack-1]);
            if(card1 == null){
                if(card.getValue() == 1){
                    return true;
                }
            }else{
                return followsAce(card1,card);
            }
            //normal
        }else if(stack >=5 && stack <=11){
            Card card1 = getLastCard(faceup[stack-5]);
            if(card1 != null){
                return followsCard(card1,card);
            }else{
                if(card.getValue() ==13){
                    return true;
                }
            }
        }
        return false;
    }
    //Returns the actual last card in an array.
    private Card getLastCard(Card[] cards){
        for(int i = 0; i < cards.length; i++){
            if(cards[i] == null){
                if(i >0){
                    return cards[i-1];
                }
            }
        }
        return null;
    }
    //Grabs the last card index in an Array
    private int getLastCardIndex(Card[] cards){
        for(int i = 0; i < cards.length; i++){
            if(cards[i] == null){
                if(i >0){
                    return i-1;
                }
            }
        }
        return -1;
    }
    //Checks if a card belongs in a normal pile.
    private boolean followsCard(Card card, Card card2){
        if(card.getSuit() == Suit.CLUB || card.getSuit() == Suit.SPADE){
            if(card2.getSuit() == Suit.DIAMOND || card2.getSuit() == Suit.HEART){
                return card.getValue() -1 == card2.getValue();
            }else{
                return false;
            }
        }else{
            if(card2.getSuit() == Suit.CLUB || card2.getSuit() == Suit.SPADE){
                return card.getValue() -1 == card2.getValue();
            }else{
                return false;
            }
        }
    }
    //Checks if a card belongs in the ace pile.
    private boolean followsAce(Card card, Card card2){
        if(card.getSuit() == card2.getSuit()){
                return card.getValue() +1 == card2.getValue();
        }
        return false;
    }

    //Returns a String for the field to be displayed.
    public String displayField(){
        String str = "Aces:\n";

        for(int i = 0; i<4; i++){
            str = str + (i +1)+":";
            if(aces[i][0] != null) {
                for (int j = 0; j < 15; j++) {
                    if (aces[i][j] != null) {
                        str = str + "[" + aces[i][j].getCard() + "]";
                    }
                }
            }else{
                str = str +"[  ]";
            }
            str = str + "\n";
        }
        str = str + "Field:\n";
        for(int i = 0; i<7; i++){
            str = str + (i +5)+":";
            for(int j = 0; j < 7; j++){
                if(facedown[i][j] != null){
                    str = str +"[XX]";
                }
            }
            for(int j = 0; j < 13; j++){
                if(faceup[i][j] != null){
                    str = str+"[" +faceup[i][j].getCard() + "]";
                }
            }
            str = str + "\n";
        }
        str = str + "Hand:\n";
        str = str +"12:";
        for(int i = 0; i < 3; i++){
            if(playable[i] != null){
                str = str + "[" + playable[i].getCard() + "]";
            }else{
                str = str + "[  ]";
            }
        }
        return str;
    }
}
