package L2.Envy.Sol.DeckManager;

import java.util.Comparator;

/**
 * Created by berry on 12/29/2016.
 */
public class Card {
    private Suit suit;
    private int value;
    private String card;

    public Card(Suit suit, int value) {
        this.suit = suit;
        this.value = value;
        String str = "";
        switch (value) {
            case 1:
                str = str + "A";
                break;
            case 11:
                str = str + "J";
                break;
            case 12:
                str = str + "Q";
                break;
            case 13:
                str = str + "K";
                break;
            default:
                str = str + value;
                break;
        }
        switch (suit) {
            case HEART:
                str = str + "H";
                break;
            case SPADE:
                str = str + "S";
                break;
            case DIAMOND:
                str = str + "D";
                break;
            case CLUB:
                str = str + "C";
                break;
        }
        card = str;
    }

    public Suit getSuit() {
        return suit;
    }

    public int getValue() {
        return value;
    }

    public String getCard() {
        return card;
    }
    public static class CompId implements Comparator<Card> {
        @Override
        public int compare(Card arg0, Card arg1) {
            return Integer.compare(arg0.getValue(), arg1.getValue());
        }
    }
}
